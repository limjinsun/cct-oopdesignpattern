/*
 * This is CCT 3rd year OOP- Desing pattern assignment. Group Project. 
 * 2015205 James Kapala
 * 2014399 Thandolwethu Bhebhe
 * 2015158 Jinsun Lim
 */
package designpatternca;

import java.math.BigDecimal;
import java.math.RoundingMode;

// refrence - reason you have to use BigDecimal for currency https://goo.gl/QJia8W
/**
 * This Class is collection of methods for 'BigDecimal' value in the program.
 * Methods can be used in other classes. so It has been declared as 'public' and 'static'.
 * In this program, every transaction related to currency value has been used 'BigDecimal' rather than 'double' or 'int'.
 */
public class BigDecimalBehavior {
	 /**
     * This methods return Random BigDecimal Value in the specific range.
     * @param M maximum String value of range. e.g "10.00"
     * @param m minimum String value of range. e.g "1.05"
     * @return  Random BigDecimal Value in the specific range
     */
    public static BigDecimal randomValue(String M, String m) {
        BigDecimal max = new BigDecimal(M);
        BigDecimal min = new BigDecimal(m);
        BigDecimal range = max.subtract(min);
        BigDecimal result = min.add(range.multiply(new BigDecimal(Math.random())));
        return result;
    }
	
	
	// reference https://docs.oracle.com/javase/7/docs/api/java/math/RoundingMode.html
	/**
	 * This methods return Rounded BigDecimal Value.
	 * @param b	bigDecimal value of input 
	 * @return  Rounded BigDecimal value.
	 */
    public static BigDecimal round(BigDecimal b) {
        return b.setScale(2, RoundingMode.CEILING);
    }
}
