/*
 * This is CCT 3rd year OOP- Desing pattern assignment. Group Project.
 * 2015205 James Kapala
 * 2014399 Thandolwethu Bhebhe
 * 2015158 Jinsun Lim
 */
package designpatternca;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * This class deals with Manual-Trading. the class implements Trading interface. 
 * c1 and c2 variable represent two company who trades each other. 
 */
public class ManualTrading implements Trading {

	private Company c1;
	private Company c2;
	
	private Company[] companiesSet1;
	/**
	 * This method is used for initializing class variables. 
	 * @param companiesSet1 CompanyArray from DesignPatternCA class.
	 * @param scanner		scanner object for showing menus.
	 */
	@Override
	public void showFirstConsole(Company[] companiesSet1, Scanner scanner) {

		System.out.println("** Please Select first Company.. \n1>BigA\n2>BigB\n3>BigC\n");
		int input1 = scanner.nextInt();
		System.out.println("** Please Select Second Company.. \n1>BigA\n2>BigB\n3>BigC\n");
		int input2 = scanner.nextInt();

		Map<Integer, Company> userInput = new HashMap<Integer, Company>();
		userInput.put(1, companiesSet1[0]);
		userInput.put(2, companiesSet1[1]);
		userInput.put(3, companiesSet1[2]);

		this.c1 = userInput.get(input1);
		this.c2 = userInput.get(input2);
		this.companiesSet1 = companiesSet1;
	}

	/**
	 * This method called method from TradingLogics class for manual trading.
	 */
	@Override
	public void doTrading() {
		TradingLogics.makeTwoCompanyTradeEachOther(c1, c2);
	}

	/**
	 * This method showing options after Trading.
	 * @param scanner Scanner Object for user input.	
	 */
	@Override
	public void showFinalConsole(Scanner scanner) {


		int choice = 0;
		do {
			System.out.println("\n** Choose the options..\n1>Stocks of each depots.\n2>Total cost of purchased on this trade.\n3>Total cost of sold on this trade.\n4>Profit and loss of all company.\n5>End.\n");
			choice = scanner.nextInt();
			switch (choice) {
				case 1:
					// To check every depots stocs.
					System.out.println("Total Depot's Product Review Start --");
					TradingLogics.getTotalDepotsStocks(companiesSet1);
					System.out.println("Total Depot's Product Review End --");
					break;
				case 2:
					// To show purchase cost for two companies.
					System.out.println("Total cost of Buying of " + c1.getCompanyName() + " :" + c1.getTotalDepotsForeignBoughtCost());
					System.out.println("Total cost of Buying of " + c2.getCompanyName() + " :" + c2.getTotalDepotsForeignBoughtCost());
					break;
				case 3:
					// To show selling cost for two companies.
					System.out.println("Total cost of Selling of " + c1.getCompanyName() + " :" + c1.getTotalDepotsNativeSoldCost());
					System.out.println("Total cost of Selling of " + c2.getCompanyName() + " :" + c2.getTotalDepotsNativeSoldCost());
					break;
				case 4:
					// To show each companies detaild information.
					for (Company c : companiesSet1) {
						System.out.println(c);
					}
					break;
				case 5:
					break;
				default:
					System.out.println("Thanks");
					break;
			}
		} while (choice != 5);

		// scanner.close();
		// Fisrt approch was close scanner in this class but I couldn't. and got error 
		// and I found this. 
		// https://stackoverflow.com/questions/13042008/java-util-nosuchelementexception-scanner-reading-user-input?answertab=votes#tab-top
	}
}
