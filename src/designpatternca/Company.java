/*
 * This is CCT 3rd year OOP- Desing pattern assignment. Group Project. 
 * 2015205 James Kapala
 * 2014399 Thandolwethu Bhebhe
 * 2015158 Jinsun Lim
 */
package designpatternca;

import java.math.BigDecimal;

/**
 * This is Company Class. 
 * Company hold total profit and amount of selling and amount of buying out of whole depots.
 */
public class Company {

    private String companyName;
    protected Depot depot[];
    private BigDecimal totalProfit;
    private BigDecimal totalSold;
    private BigDecimal totalBought;
	
	
	// Dependency Injection Design Pattern used. @see https://stackoverflow.com/questions/130794/what-is-dependency-injection
	/**
	 * This method is constructor of Company class. 
	 * Parameters has been used for initializing company and Depots.
	 * @param name String value of company Name,
	 * @param nativeProduct Enum value of company's native product.
	 * @param depotCount Counts of depots in the company.
	 * @param minNative Minimum value of native product in depots.
	 * @param maxNative Maximum value of native product in depots.
	 * @param minForeign Minimum value of foreign product in depots.
	 * @param maxForeign Maximum value of foreign product in depots.
	 */ 
    public Company (String name, Product nativeProduct, int depotCount, int minNative, int maxNative, int minForeign, int maxForeign){
        this.companyName = name;
        this.depot = new Depot[depotCount];

        for (int i=0; i<depotCount; i++){
            BigDecimal costOfProduct = BigDecimalBehavior.randomValue("2.00","1.00"); // see BigDecimalBehavior Class.
            BigDecimal costOfDelivery = BigDecimalBehavior.randomValue("1.50","1.00");
            BigDecimal cash = BigDecimalBehavior.randomValue("100.00","55.00");
			// Builder pattern used for initializing depots.
            depot[i] = new DepotBuilder().setNativeProduct(nativeProduct).setMinNative(minNative).setMaxNative(maxNative).setMinForeign(minForeign).setMaxForeign(maxForeign).setCostOfProduct(costOfProduct).setCostOfDelivery(costOfDelivery).setCash(cash).createDepot();
        }
    }
	/**
	 * This method return BigDecimal value of profit from every depot in the Company.
	 * @return BigDecimal value of profit calculated.
	 */
    public BigDecimal getTotalDepotsProfit (){
        BigDecimal totalProfitOfDepots = new BigDecimal("0");
        for(int i=0; i<this.depot.length; i++){
            totalProfitOfDepots = totalProfitOfDepots.add(this.depot[i].getProfit(this.depot[i].getOriginalCash(), this.depot[i].getCash()));
        }
        return totalProfitOfDepots;
    }

	/**
	 * This method return BigDecimal value of whole selling from every depot in the Company.
	 * @return BigDecimal value of whole selling calculated.
	 */
    public BigDecimal getTotalDepotsNativeSoldCost (){
        BigDecimal totalSoldCost = new BigDecimal("0");
        for(int i=0; i<this.depot.length; i++){
            totalSoldCost = totalSoldCost.add(this.depot[i].getNativeSoldTotalCost());
        }
        return totalSoldCost;
    }
	
	/**
	 * This method return BigDecimal value of whole buying from every depot in the Company.
	 * @return BigDecimal value of whole buying calculated.
	 */
    public BigDecimal getTotalDepotsForeignBoughtCost(){
        BigDecimal totalBoughtCost = new BigDecimal("0");
        for(int i=0; i<this.depot.length; i++){
            totalBoughtCost = totalBoughtCost.add(this.depot[i].getForeignBoughtTotalCost());
        }
        return totalBoughtCost;
    }
	
	/**
	 * This method return String value of specific details including Name, Total Number of Depots, Total Profit, Total sold, and Total bought.
	 * @return String value of specific detail of Company class.
	 */
    public String toString() {
        totalProfit = getTotalDepotsProfit();
        totalSold = getTotalDepotsNativeSoldCost();
        totalBought = getTotalDepotsForeignBoughtCost();
        return "\nCompany Info\n" + "Company name : " + getCompanyName() + "\nTotal profit of every depots : " + totalProfit + "\nTotal sold of every depots : " + totalSold + "\nTotal bought of every depots : " + totalBought;
    }
	
	/**
	 * This method return String value of name of Company.
	 * @return Name of Company
	 */
	public String getCompanyName() {
		return companyName;
	}
	
}