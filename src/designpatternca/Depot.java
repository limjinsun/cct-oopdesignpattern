/*
 * This is CCT 3rd year OOP- Desing pattern assignment. Group Project. 
 * 2015205 James Kapala
 * 2014399 Thandolwethu Bhebhe
 * 2015158 Jinsun Lim
 */
package designpatternca;

import java.math.BigDecimal;
import java.util.EnumMap;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;
/**
 * This is Depot Class. Every variable declared as private for 'encapsulation' principle of OOP.
 * Depot also has unique identify number that generated when it initialized.
 * @author rainbowtape
 */
public class Depot {
    
    static AtomicInteger nextId = new AtomicInteger();
    private int depotId; 
    private Product nativeProduct;

    private int minNative;
    private int maxNative;
    private int nativeCount;

    private int minForeign;
    private int maxForeign;
    private int foreignCount;

    private final BigDecimal originalCash; // 'final' identifyer used as it won't change. 
    private BigDecimal cash;
    private BigDecimal costOfProduct;
    private BigDecimal costOfDelivery;
    private BigDecimal totalCost;
    private BigDecimal nativeSoldTotalCost;
    private BigDecimal foreignBoughtTotalCost;

    private boolean buyingRecord = false;

	// For Way we count product in depot, Map collection used. in a Map, enum value can be stored as Key, and intergr value for the Value. 
    EnumMap<Product, Integer> productCountsChart;
	
	/**
	 * This method is constructor of Depot.
	 * in order to initialize depot. Depot builder class will be used. 
	 * @see DepotBuilder class. 
	 * @param nativeProduct		enum value of native product of depot
	 * @param minNative			minimum amount of native product in depot
	 * @param maxNative			maximum amount of native product in depot
	 * @param minForeign		minimum amount of foreign product in depot
	 * @param maxForeign		maximum amount of foreign product in depot
	 * @param costOfProduct		cost of product
	 * @param costOfDelivery	cost of delivery
	 * @param cash				amount of cash in depot
	 */
    public Depot(Product nativeProduct, int minNative, int maxNative, int minForeign, int maxForeign, BigDecimal costOfProduct, BigDecimal costOfDelivery, BigDecimal cash) {
        this.depotId = nextId.incrementAndGet();
		
        this.nativeProduct = nativeProduct;
        this.minNative = minNative;
        this.maxNative = maxNative;
        int randomNativeCount = ThreadLocalRandom.current().nextInt(minNative, maxNative + 1);
		int randomForeignCount = ThreadLocalRandom.current().nextInt(minForeign, (maxForeign + 1) / 4);
        this.minForeign = minForeign;
        this.maxForeign = maxForeign;
        this.costOfProduct = BigDecimalBehavior.round(costOfProduct);
        this.costOfDelivery = BigDecimalBehavior.round(costOfDelivery);
        this.cash = BigDecimalBehavior.round(cash);
        this.originalCash = BigDecimalBehavior.round(cash);
        BigDecimal b = costOfProduct;
        this.totalCost = BigDecimalBehavior.round(b.add(costOfDelivery));
        this.productCountsChart = new EnumMap<>(Product.class);
        for (Product product : Product.values()) {
            if (nativeProduct == product) {
                productCountsChart.put(product, randomNativeCount);
            } else {
                productCountsChart.put(product, randomForeignCount);
            }
        }
        this.nativeCount = productCountsChart.get(nativeProduct);
        this.foreignCount = getForeignProductSum(nativeProduct, this.productCountsChart); 
        this.nativeSoldTotalCost = new BigDecimal("0");
        this.foreignBoughtTotalCost = new BigDecimal("0");
    }
	
	/**
	 * This method return BigDecimal value of depot's profit. 
	 * It will be calculated using subtraction.
	 * @param originalCash	Original cash BigDecimal value
	 * @param cash			Current Cash BigDecimal value
	 * @return				BigDecimal value of depot's profit		
	 */
    public BigDecimal getProfit (BigDecimal originalCash, BigDecimal cash){
        BigDecimal result;
        result = cash.subtract(originalCash);
        return result;
    }
	
	/**
	 * This method return int value of sum of foreign products.
	 * @param product				enum value of native product
	 * @param productCountsChart	map you want to calculate of foreign products in
	 * @return						int value of sum of foreign products.
	 */
    public int getForeignProductSum(Product product, EnumMap<Product, Integer> productCountsChart ) {
        int sum = 0;
        for (EnumMap.Entry<Product, Integer> entry : productCountsChart.entrySet()) {
            if (entry.getKey() != product) {
                sum += entry.getValue();
            }
        }
        return sum;
    }

    /* getter and setters */

    public Product getNativeProduct() {
        return nativeProduct;
    }
    public void setForeignCount(int foreignCount) {
        this.foreignCount = foreignCount;
    }
    public void setNativeCount(int nativeCount) {
        this.nativeCount = nativeCount;
    }
    public int getNativeCount() {
        return nativeCount;
    }
    public int getForeignCount() {
        return foreignCount;
    }
    public BigDecimal getTotalCost() {
        return totalCost;
    }
    public BigDecimal getCash() {
        return cash;
    }
    public void setCash(BigDecimal cash) {
        this.cash = cash;
    }
    public boolean getBuyingRecord() {
        return buyingRecord;
    }
    public void setBuyingRecord(boolean buyingRecord) {
        this.buyingRecord = buyingRecord;
    }
    public BigDecimal getOriginalCash() {
        return originalCash; 
    }
    public BigDecimal getNativeSoldTotalCost() {
        return nativeSoldTotalCost;
    }
    public void setNativeSoldTotalCost(BigDecimal nativeSoldTotalCost) {
        this.nativeSoldTotalCost = nativeSoldTotalCost;
    }
    public BigDecimal getForeignBoughtTotalCost() {
        return foreignBoughtTotalCost;
    }
    public void setForeignBoughtTotalCost(BigDecimal foreignBoughtTotalCost) {
        this.foreignBoughtTotalCost = foreignBoughtTotalCost;
    }
	/**
	 * This method return String value detailed info of depot.
	 * @return	Detail information of depot. 
	 */
    public String toString() {
        return "* Depot info ---\n" + "Depot-id : " + depotId + "\nNative-product : " + nativeProduct + "\noriginal cash : " + this.originalCash + "\ncash : " + this.cash + "\ncost of Product : " + this.costOfProduct + "\ncost of Delivery : " + this.costOfDelivery + "\ntotalCost : " + this.totalCost + "\nnative product storage : " + this.nativeCount + "\nnative sold cost total : " + this.nativeSoldTotalCost + "\nforeign product storage : " + this.foreignCount + "\nforeign bought cost total : " + this.foreignBoughtTotalCost;
    }
}
