	/*
 * This is CCT 3rd year OOP- Desing pattern assignment. Group Project.
 * 2015205 James Kapala
 * 2014399 Thandolwethu Bhebhe
 * 2015158 Jinsun Lim
 */
package designpatternca;

import java.util.Scanner;

/**
 * This class deals with trading functions. Facade design pattern used.
 * 
 */
public class TradingManager {
	
	private Trading manualTrading;
	private Trading randomTrading;
	
	private Company[] companiesSet1;
	
	public TradingManager (Company[] companiesSet1){
		this.manualTrading = new ManualTrading();
		this.randomTrading = new RandomTrading();
		this.companiesSet1 = companiesSet1;
	}
	// Manual Trading.
	public void doItManual (){
		Scanner scanner = new Scanner(System.in);
		manualTrading.showFirstConsole(companiesSet1, scanner);
		manualTrading.doTrading();
		manualTrading.showFinalConsole(scanner);
		scanner.close();
	}
	// Random Trading.
	public void doItRandom (){
		Scanner scanner = new Scanner(System.in);
		randomTrading.showFirstConsole(companiesSet1,scanner);
		randomTrading.doTrading();
		randomTrading.showFinalConsole(scanner);
		scanner.close();
	}
}
