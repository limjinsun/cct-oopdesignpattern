/*
 * This is CCT 3rd year OOP- Desing pattern assignment. Group Project. 
 * 2015205 James Kapala
 * 2014399 Thandolwethu Bhebhe
 * 2015158 Jinsun Lim
 */
package designpatternca;

import java.math.BigDecimal;
/**
 * This class is DepotBuilder Class.
 * Builder design pattern used.
 */
public class DepotBuilder {

	private Product nativeProduct;
	private int minNative;
	private int maxNative;
	private int minForeign;
	private int maxForeign;
	private BigDecimal costOfProduct;
	private BigDecimal costOfDelivery;
	private BigDecimal cash;

	public DepotBuilder() {
	}

	public DepotBuilder setNativeProduct(Product nativeProduct) {
		this.nativeProduct = nativeProduct;
		return this;
	}

	public DepotBuilder setMinNative(int minNative) {
		this.minNative = minNative;
		return this;
	}

	public DepotBuilder setMaxNative(int maxNative) {
		this.maxNative = maxNative;
		return this;
	}

	public DepotBuilder setMinForeign(int minForeign) {
		this.minForeign = minForeign;
		return this;
	}

	public DepotBuilder setMaxForeign(int maxForeign) {
		this.maxForeign = maxForeign;
		return this;
	}

	public DepotBuilder setCostOfProduct(BigDecimal costOfProduct) {
		this.costOfProduct = costOfProduct;
		return this;
	}

	public DepotBuilder setCostOfDelivery(BigDecimal costOfDelivery) {
		this.costOfDelivery = costOfDelivery;
		return this;
	}

	public DepotBuilder setCash(BigDecimal cash) {
		this.cash = cash;
		return this;
	}

	public Depot createDepot() {
		return new Depot(nativeProduct, minNative, maxNative, minForeign, maxForeign, costOfProduct, costOfDelivery, cash);
	}
	
}
