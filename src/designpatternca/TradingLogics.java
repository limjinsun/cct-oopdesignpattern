/*
 * This is CCT 3rd year OOP- Desing pattern assignment. Group Project. 
 * 2015205 James Kapala
 * 2014399 Thandolwethu Bhebhe
 * 2015158 Jinsun Lim
 */
package designpatternca;
import java.math.BigDecimal;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 * This Class are dealing with trading functionalities. 
 * Core function part of Project
 * 
 */
public class TradingLogics {
	// class constructor.
    public TradingLogics() {
    }
	/**
	 * This method return Company object chosen by random number. 
	 * @param array Company array
	 * @return		Company object randomly chosen.
	 */
    public static Company getRandomCompany(Company[] array) {
        int rnd = new Random().nextInt(array.length);
        return array[rnd];
    }

	/**
	 * This method make totally random trade among companies.
	 * @param companies company array which want to trade.
	 */
    public static void totallyRandomTrade(Company[] companies) {
		
		// randomly chosen company.
        Company randomBuyerCompany = getRandomCompany(companies);
        Company temp = getRandomCompany(companies);
        while(temp == randomBuyerCompany){
            temp = getRandomCompany(companies);
        } 
		// make sure that two "diferrent" compnaies.
        Company randomSellerCompany = temp;
		
		// randimly chosen depots.
        int rndB = ThreadLocalRandom.current().nextInt(0, 99 + 1);
        int rndS = ThreadLocalRandom.current().nextInt(0, 99 + 1);
        
		// trade with two depots.
        if (checkAvailabilityOfTrade(randomBuyerCompany.depot[rndB], randomSellerCompany.depot[rndS])) {
            buyAndSell(randomBuyerCompany.depot[rndB], randomSellerCompany.depot[rndS]);
        } else {
            System.out.println("-- buyer : " + randomBuyerCompany.depot[rndB]);
            System.out.println("-- seller : " + randomSellerCompany.depot[rndS] + "\n");
        }
    }
	
	
	/**
	 * This method make Buyer company buy product from Two seller companies. 
	 * In this program, this method has not been used.
	 * @param buyerCompanyA		buyer company
	 * @param sellerCompanyB	seller company 1
	 * @param sellerCompanyC	seller company 2
	 */
    public static void makeCompanyBuyTheOtherTwo(Company buyerCompanyA, Company sellerCompanyB, Company sellerCompanyC) {
        for (int i = 0; i < 100; i++) {
            if(buyerCompanyA.depot[i].getCash().compareTo(new BigDecimal("55")) == 1){ // Preventing buyer who has only little money trying to trade endlessly
                makeDepotBuyProductFromCompany(buyerCompanyA.depot[i], sellerCompanyB);
                if(buyerCompanyA.depot[i].getCash().compareTo(new BigDecimal("55")) == 1){
                    makeDepotBuyProductFromCompany(buyerCompanyA.depot[i], sellerCompanyC);
                }
            }
        }
    }
	
	/**
	 * This method make two companies trade each other. "makeDepotBuyProductFromCompany" method called.
	 * @param c1 company1
	 * @param c2 company2
	 */
	public static void makeTwoCompanyTradeEachOther(Company c1, Company c2){
		for (int i = 0; i < 100; i++) {
            if(c1.depot[i].getCash().compareTo(new BigDecimal("55")) == 1){ // Preventing buyer who has only little money trying to trade endlessly
                makeDepotBuyProductFromCompany(c1.depot[i], c2);
                if(c2.depot[i].getCash().compareTo(new BigDecimal("55")) == 1){
                    makeDepotBuyProductFromCompany(c2.depot[i], c1);
                }
            }
        }
	}

	/**
	 * This method make depot buy product from specific company. Every depot must try to buy until trade made.
	 * @param buyerDepot	buyer depot
	 * @param sellerCompany	seller company 
	 */
    public static void makeDepotBuyProductFromCompany(Depot buyerDepot, Company sellerCompany) {
        while(!buyerDepot.getBuyingRecord()){ // making sure depot trade at least once. if trade fail, keep trying to find other depot available.
            int randomNum = ThreadLocalRandom.current().nextInt(0, 99 + 1); // randomly choose depot's no.
            if(checkAvailabilityOfTrade(buyerDepot, sellerCompany.depot[randomNum])){
                buyAndSell(buyerDepot, sellerCompany.depot[randomNum]);
                buyerDepot.setBuyingRecord(true); // setting up buying recode so that it can stop trying to buy in while loop.
            } else {
                System.out.println("-- buyer : " + buyerDepot);
                System.out.println("-- seller : " + sellerCompany.depot[randomNum] + "\n");
            }
        }
		
        buyerDepot.setBuyingRecord(false); // default resetting buying record for current depot.
    }
	
	/**
	 * This method show every depot's stocks. 
	 * @param companies	company array.
	 */
	public static void getTotalDepotsStocks(Company[] companies) {
		for (Company c : companies) {
			for (int i = 0; i < c.depot.length; i++) {
				int j = i + 1; // index number + 1 for depot counting.
				System.out.println(c.getCompanyName() + " - " + j + " Depot: " + c.depot[i].productCountsChart.entrySet());
			}
		}
	}
	
	/**
	 * This method return boolean value of availability of two depots.
	 * @param buyerDepot	buying depot
	 * @param sellerDepot	selling depot
	 * @return				boolean value.
	 */
    public static boolean checkAvailabilityOfTrade(Depot buyerDepot, Depot sellerDepot) {
        boolean flag;
        BigDecimal productPrice = sellerDepot.getTotalCost();
        if (buyerDepot.getCash().compareTo(productPrice) == 1 && buyerDepot.getCash().compareTo(productPrice.add(new BigDecimal("50"))) == 1) {
            if (sellerDepot.getCash().compareTo(new BigDecimal("100").subtract(productPrice)) == -1) {
                if (sellerDepot.getNativeCount() > 15) {
                    if(buyerDepot.getForeignCount() < 40) {
                        System.out.println("## Ok! Deal!");
                        flag = true;
                    } else {
                        System.out.println("!! Trade-fail : Buyer's Foreign Storage is not suitable for trading"); // maximum storage restriction of depot.
                        flag = false;
                    }
                } else {
                    System.out.println("!! Trade-fail : Seller's Native Storage is not suitable for trading"); // minimum storage restriction of depot.
                    flag = false;
                }
            } else {
                System.out.println("!! Trade-fail : Seller's Cash is going to be too much"); // maximum cash restriction of depot.
                flag = false;
            }
        } else {
            System.out.println("!! Trade-fail : buyer's cash is not enough to buy"); // minimum cash restriction of depot.
            flag = false;
        }
        return flag;
    }

    /*!!!!! Core Buy and Sell Mechanism !!!!!*/
	/**
	 * This method is core mechanism of buy and sell process in between two depots. The method has been used in almost every trade function.
	 * @param buyerDepot	buyer depot.
	 * @param sellerDepot	seller depot.
	 */
    public static void buyAndSell(Depot buyerDepot, Depot sellerDepot) {

        Product tradeProduct = sellerDepot.getNativeProduct();
        BigDecimal productPrice = sellerDepot.getTotalCost();
		
        //* 1. buyerDepot's cash reduced 
        buyerDepot.setCash(BigDecimalBehavior.round(buyerDepot.getCash().subtract(productPrice)));
		//* 1-1. Update buying price of the depot
        buyerDepot.setForeignBoughtTotalCost(buyerDepot.getForeignBoughtTotalCost().add(productPrice));
		
        //* 2. sellerDepot's stock reduced
        sellerDepot.productCountsChart.put(tradeProduct, sellerDepot.productCountsChart.get(tradeProduct) - 1);
		//* 2-1. set up Native product count variable in the depot.
        sellerDepot.setNativeCount(sellerDepot.productCountsChart.get(tradeProduct));
		
        //* 3. buyerDepot's stock increased
        buyerDepot.productCountsChart.put(tradeProduct, buyerDepot.productCountsChart.get(tradeProduct) + 1);
		//* 3-1. set up Foreign product count variable in the depot. 
        buyerDepot.setForeignCount(buyerDepot.getForeignProductSum(buyerDepot.getNativeProduct(), buyerDepot.productCountsChart));
		
        //* 4. sellerDepot's cash increased and update sold price of depot
        sellerDepot.setCash(BigDecimalBehavior.round(sellerDepot.getCash().add(productPrice)));
		//* 4-1. Update selling price of the depot
        sellerDepot.setNativeSoldTotalCost(sellerDepot.getNativeSoldTotalCost().add(productPrice));

		// show trade result.
        System.out.println("-- buyer : " + buyerDepot);
        System.out.println("-- seller : " + sellerDepot);
        System.out.println("## trade end ##" + "\n");
    }
}
