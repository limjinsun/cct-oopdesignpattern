/*
 * This is CCT 3rd year OOP- Desing pattern assignment. Group Project.
 * 2015205 James Kapala
 * 2014399 Thandolwethu Bhebhe
 * 2015158 Jinsun Lim
 */
package designpatternca;

import java.util.Scanner;

/**
 * 
 * 
 * This class is main functioning class in the package.
 * 
 * 
 */
public class DesignPatternCA {

	public static void main(String[] args) {
		mainMenu();
	}

	/**
	 * This method set up the environment for application and show first menu.
	 */
	private static void mainMenu() {
		
		// Initilizing companies and making it as CompanyArray. 
		Company A = new Company("BigA", Product.A, 100, 15, 50, 3, 40);
		Company B = new Company("BigB", Product.B, 100, 15, 50, 3, 40);
		Company C = new Company("BigC", Product.C, 100, 15, 50, 3, 40);

		Company[] companiesSet1 = new Company[3];

		companiesSet1[0] = A;
		companiesSet1[1] = B;
		companiesSet1[2] = C;

		
		// Showing very fisrt menu and take a input and run methods acordingly. 
		System.out.println("** Please Select Menu..\n\n1>Manual Trading \n2>Random Trading \n3>Stocks of each depots \n");
		
		Scanner scanner = new Scanner(System.in);
		int choice = scanner.nextInt();
		
		// Trading fucntion has been used as Facade desgin pattern. 
		TradingManager manager = new TradingManager(companiesSet1);
		
		switch (choice) {
			case 1:
				manager.doItManual();
				break;
			case 2:
				manager.doItRandom();
				break;
			case 3:
				TradingLogics.getTotalDepotsStocks(companiesSet1);
				mainMenu();
			default:
				System.out.println("return to main");
				mainMenu();
		}
		scanner.close();
		
	}
}
