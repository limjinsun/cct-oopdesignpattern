/*
 * This is CCT 3rd year OOP- Desing pattern assignment. Group Project. 
 * 2015205 James Kapala
 * 2014399 Thandolwethu Bhebhe
 * 2015158 Jinsun Lim
 */
package designpatternca;

/**
 * For calculating number of product, Enum map has been used. 
 * Product declared as enum.	
 */
public enum Product {
    // enums
    A,B,C
}
