/*
 * This is CCT 3rd year OOP- Desing pattern assignment. Group Project.
 * 2015205 James Kapala
 * 2014399 Thandolwethu Bhebhe
 * 2015158 Jinsun Lim
 */
package designpatternca;

import java.util.Scanner;

/**
 *	This is interface class for Trading function. Facade design pattern used. 
 */
public interface Trading {	
	
	public void showFirstConsole(Company[] companiesSet1, Scanner scanner);
	public void doTrading();
	public void showFinalConsole(Scanner scanner);
}
