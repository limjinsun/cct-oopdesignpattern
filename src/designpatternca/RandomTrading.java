/*
 * This is CCT 3rd year OOP- Desing pattern assignment. Group Project.
 * 2015205 James Kapala
 * 2014399 Thandolwethu Bhebhe
 * 2015158 Jinsun Lim
 */
package designpatternca;

import java.util.Scanner;

/**
 * This class deals with Random-Trading. the class implements Trading interface. 
 * number represent how many times to try to trade in between depots.
 */
public class RandomTrading implements Trading {

	private int numbers;
	private Company[] companiesSet1;

	/**
	 * This method is used for initializing class variable. 
	 * @param companiesSet1 CompanyArray from DesignPatternCA class.
	 * @param scanner		scanner object for showing menus.
	 */
	@Override
	public void showFirstConsole(Company[] companiesSet1, Scanner scanner) {
		System.out.println("** Companys and Depots will be chosen randomly.\nHow many time would you want to try to trade?\nPlease type numbers(10 ~ 1000)\n");
		numbers = scanner.nextInt();

		this.companiesSet1 = companiesSet1;
	}
	
	/**
	 * This method called method from TradingLogics class for random-trading.
	 */
	@Override
	public void doTrading() {
		for (int i = 0; i < numbers; i++) {
			TradingLogics.totallyRandomTrade(companiesSet1); // see the TradingLosics class.
		}
	}
	
	/**
	 * This method showing options after Trading.
	 * @param scanner Scanner Object for user input.
	 */
	@Override
	public void showFinalConsole(Scanner scanner) {
		int choice = 0;
		do {
			System.out.println("\n** Choose the options..\n1>Stocks of each depots.\n2>Total cost of purchased on this trade.\n3>Total cost of sold on this trade.\n4>Profit and loss of each company.\n5>End.\n");
			choice = scanner.nextInt();
			switch (choice) {
				case 1:
					System.out.println("Total Depot's Product Review Start --");
					TradingLogics.getTotalDepotsStocks(companiesSet1);
					System.out.println("Total Depot's Product Review End --");
					break;
				case 2:
					for (Company c : companiesSet1) {
						System.out.println("Total cost of Buying of " + c.getCompanyName() + " :" + c.getTotalDepotsForeignBoughtCost());
					}
					break;
				case 3:
					for (Company c : companiesSet1) {
						System.out.println("Total cost of Selling of " + c.getCompanyName() + " :" + c.getTotalDepotsNativeSoldCost());
					}
					break;
				case 4:
					for (Company c : companiesSet1) {
						System.out.println(c);
					}
					break;
				case 5:
					break;
				default:
					System.out.println("Thanks");
					break;
			}
		} while (choice != 5);
	}
}
