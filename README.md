# CCT 3rd year project - OOP design pattern.

## Description

This project is for CCT 3rd year group project. Participants are followings.
* 2015205 James Kapala
* 2014399 Thandolwethu Bhebhe
* 2015158 Jinsun Lim

## Java Documentation

The documentation overview is in the [this](https://rbwtp.gitlab.io/cct-oopdesignpattern/).

## Class Diagram

![alt text](https://i.imgur.com/kWiGW4M.png)

